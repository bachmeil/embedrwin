This directory is used to hold the LDC compiler. If you call `ldc.install`, the LDC compiler
will be downloaded and unzipped into this directory, and you do not need to pass the `ldc`
argument for compilation.
