# Overview

This is a testing release of the embedr package that has support for
64-bit Windows. (32-bit can easily be added, and it will in the future.)
This functionality will be added to embedr in the future.

Please visit the [Gitter page](https://gitter.im/dlang-embedr/Lobby) for the latest updates on the project status. You can also post questions or issues there if you prefer.

# Status

I plan to add the following functionality.

- A function that installs LDC. In that case, you won't need to supply the path for LDC, because the package will know where to find it. This is important functionality for those not accustomed to installing compilers.
- A function that copies R.dll into the package. This is not necessary, but in that case R.dll is copied into the current working directory, which takes up a lot of space if you compile D code in many directories.
- A function that does both.

When this is done, it will be easier to write and call D functions on Windows than it is to call C or Fortran functions. When everything has been tested, it will be merged into embedr.

# Installation

The LDC compiler is required. It is possible that DMD will work, but that
is not supported at this time.

You should install the package using devtools:

```
library(devtools)
install_bitbucket("bachmeil/embedrwin")
```

For some reason I also had to install the `git2r` package as well. If you
get an error message, you may have to do the same.

Once it is installed, you can compile the file with your D code using the
`compile` function. `compile` takes two arguments, the name of the D file
(with no extension) and the location of your LDC installation (the full
path to \bin).

# Test the Installation

You can test it out by putting the following code into the file librtest.d:

```
import embedr.r;
mixin(createRLibrary("rtest"));

import std.math;

export extern(C) {
  Robj transform(Robj rx) {
    auto x = RVector(rx);
	double[] y = [log(x[0]), log(x[1])];
	double result = y[0] + y[1];
	return result.robj;
  }
}
```

Then in R, from the same directory as librtest.d, create and load the DLL
with the `compile` function:

```
compile("librtest", "C:\\Users\\lance\\ldc64\\ldc2-1.3.0-beta2-win64-msvc\\bin")
```

The second argument points to my installation of LDC. Yours will be different.
Note the use of double slashes in the path.

After that is done compiling and loading the DLL, you can test it out:

```
.Call("transform", c(2.0, 3.0))
```

It will return 1.791759 if everything worked.